<?php
require_once('Animal.php');

class Ape extends Animal{
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";
    public function yell(){
        echo "Yell : Auoo <br> <br>";
    }
}

?>